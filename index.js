// Теоретичні питання
// 1. Як можна оголосити змінну у Javascript?
// Оголосити змінну можна завдяки ключовим словам: var (не використовується), let і const та потрібно назвати змінну.

// 2. У чому різниця між функцією prompt та функцією confirm?
// Ф-ція prompt - в модальному вікні користувач має вести дані власноруч. 
// При використанні ф-ції confirm - користувач має або погодитись, з повідомленням в модальному вікні, або відхилити його.

// 3. Що таке неявне перетворення типів? Наведіть один приклад.
// Автоматичне перетворення при компіляції 
// Приклад: 
// let numberA = 54; 
// let numberB = '75'; 
// console.log(numberA + numberB); 
// numberA = 54 - стане рядком при компіляції


// 1. Оголосіть дві змінні: admin та name. 
// Встановіть ваше ім'я в якості значення змінної name. 
// Скопіюйте це значення в змінну admin і виведіть його в консоль.

const name = 'Olha';
let admin = name;
console.log(admin);

// 2. Оголосити змінну days і ініціалізувати її числом від 1 до 10. 
// Перетворіть це число на кількість секунд і виведіть на консоль.

let days = 10;
let secInDay = 86400;
console.log(days * secInDay);

// 3. Запитайте у користувача якесь значення і виведіть його в консоль.

let userNumber = +prompt('Enter number');
console.log(userNumber);
